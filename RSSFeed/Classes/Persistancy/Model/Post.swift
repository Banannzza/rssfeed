//
//  Post.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Post: Codable {
    
    let link: String
    let title: String
    let publicationDate: String
    let image: String?

    enum CodingKeys: String, CodingKey
    {
        case link
        case title
        case publicationDate = "pubDate"
        case image
    }
    
}


