//
//  FavoritePosts.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 10/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct FavoritePosts: FavoritePostsProtocol {
    
    private let favoritePostKey = "FavoritePosts"
    
    func isFavorite(post: Post) -> Bool {
        guard let favorites = UserDefaults.standard.dictionary(forKey: favoritePostKey)
        else {
            return false
        }
        return favorites.keys.contains(post.link)
    }
    
    func setFavorite(post: Post) {
        guard var favorites = UserDefaults.standard.dictionary(forKey: favoritePostKey) as? [String : Data]
        else {
            if let encodedData = try? JSONEncoder().encode(post) {
                UserDefaults.standard.set([post.link : encodedData], forKey: favoritePostKey)
            }
            return

        }
        if !favorites.keys.contains(post.link) {
            if let encodedData = try? JSONEncoder().encode(post) {
                favorites[post.link] = encodedData
            }
            UserDefaults.standard.set(favorites, forKey: favoritePostKey)
        }
    }
    
    func removeFavorite(post: Post) {
        guard var favorites = UserDefaults.standard.dictionary(forKey: favoritePostKey)
        else {
            return
        }
        favorites[post.link] = nil
        UserDefaults.standard.set(favorites, forKey: favoritePostKey)
    }
    
    func obtainFavoritePosts() -> [Post] {
        guard let favorites = UserDefaults.standard.dictionary(forKey: favoritePostKey) as? [String : Data] else { return [] }

        return (favorites.values.flatMap { try? JSONDecoder().decode(Post.self, from: $0) })
    }
    
}
