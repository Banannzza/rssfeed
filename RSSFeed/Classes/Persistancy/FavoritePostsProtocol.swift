//
//  FavoritePostsProtocol.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 10/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol FavoritePostsProtocol {
    
    func isFavorite(post: Post) -> Bool
    
    func setFavorite(post: Post)
    
    func removeFavorite(post: Post)
    
    func obtainFavoritePosts() -> [Post]
    
}
