//
//  RSSTransport.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class RSSTransport : NSObject {
    
    // MARK: - Property
    
    private let parser: XMLParser
    private let dispatchQueue = DispatchQueue(label: "RSSTrasportQueue")
    private let completionHandler: ([[String : String]]) -> Void
    private let interestedAttributes: [String]
    private var currentElement = ""
    private var foundCharacters = ""
    private var currentData = [String : String]()
    private var parsedData = [[String : String]]()
    private var isHeader = true
    
    // MARK: - Init
    
    init?(link: URL, completionHandler: @escaping ([[String : String]]) -> Void) {
        guard let parser = XMLParser(contentsOf: link) else { return nil }
        self.parser = parser
        self.completionHandler = completionHandler
        self.interestedAttributes = ["title", "link", "description", "content", "pubDate", "enclosure"]
        super.init()
        self.parser.delegate = self
        
        self.dispatchQueue.async {
            self.parser.parse()
        }
        
    }
    
    init?(link: URL, interestedAttributes: [String], completionHandler: @escaping ([[String : String]]) -> Void) {
        guard let parser = XMLParser(contentsOf: link) else { return nil }
        self.parser = parser
        self.completionHandler = completionHandler
        self.interestedAttributes = interestedAttributes
        super.init()
        self.parser.delegate = self
        
        self.dispatchQueue.async {
            self.parser.parse()
        }
    }
    
}

// MARK: - XMLParserDelegate

extension RSSTransport : XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        if currentElement == "item" || currentElement == "entry" {
            if !isHeader {
                parsedData.append(currentData)
            }
            
            isHeader = false
        }
        
        if !isHeader {
            if currentElement == "media:thumbnail" || currentElement == "media:content" {
                if let url = attributeDict["url"] {
                    foundCharacters += url
                }
            }
            if currentElement == "enclosure" {
                if let url = attributeDict["url"] {
                    foundCharacters += url
                }
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if !isHeader {
            if interestedAttributes.contains(currentElement) && currentElement == "description" {
                if let regex = try? NSRegularExpression(pattern: "(<img.*?src=\")(.*?)(\".*?>)", options: []) {
                    let range = NSMakeRange(0, string.count)
                    regex.enumerateMatches(in: string, options: [], range: range) { (result, _, _) -> Void in
                        let nsrange = result!.range(at: 2)
                        let start = string.index(string.startIndex, offsetBy: nsrange.location)
                        let end = string.index(start, offsetBy: nsrange.length)
                        foundCharacters += (string[start..<end])
                    }
                }
            }
            else
            if interestedAttributes.contains(currentElement) {
                foundCharacters += string
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if !foundCharacters.isEmpty {
            foundCharacters = foundCharacters.trimmingCharacters(in: .whitespacesAndNewlines)
            if currentElement == "enclosure" || currentElement == "description" {
                currentElement = "image"
            }
            currentData[currentElement] = foundCharacters
            foundCharacters = ""
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.completionHandler(self.parsedData)
    }
}
