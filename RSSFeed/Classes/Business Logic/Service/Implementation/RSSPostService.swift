//
//  RSSPostService.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class RSSPostService: RSSPostServiceProtocol {
    
    var rssLoader: RSSTransport?
    
    func loadPost(from: URL, completionHandler: @escaping (ServiceResponse<[Post]>) -> Void) {
        self.rssLoader = RSSTransport(link: from) { parsedData in
            let posts: [Post] = ObjectParser.parseObject(fromJson: ["data" : parsedData])
            if posts.count > 0 {
                completionHandler(.success(posts))
            }
            else {
                completionHandler(.fail(nil))
            }
        }
        if self.rssLoader == nil {
            completionHandler(.fail(nil))
        }
    }
    
}
