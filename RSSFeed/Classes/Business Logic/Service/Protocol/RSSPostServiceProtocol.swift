//
//  RSSPostServiceProtocol.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol RSSPostServiceProtocol: AnyObject {
    
    /// Загрузка постов по ссылке
    ///
    /// - Parameter from: ссылка
    /// - Parameter completionHandler: ответ от сервера
    func loadPost(from: URL, completionHandler: @escaping (ServiceResponse<[Post]>) -> Void)
    
}
