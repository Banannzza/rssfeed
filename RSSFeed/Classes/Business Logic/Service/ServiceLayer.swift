//
//  ServiceLayer.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class ServiceLayer {
    
    static let shared = ServiceLayer()
    let rssReader: RSSPostServiceProtocol
    let favoritePosts: FavoritePostsProtocol
    
    private init() {
        self.rssReader = RSSPostService()
        self.favoritePosts = FavoritePosts()
    }
    
}
