//
//  RSSFeedPresentationModel.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class RSSFeedPresentationModel {
    
    // MARK: - Property
    
    var urlPool: [URL]
    var postViewModels: [[Post]]
    weak var delegate: RSSFeedPresentationModelDelegate?
    
    // MARK: - Init
    
    init(urlPool: [URL]) {
        self.urlPool = urlPool
        self.postViewModels = [[Post]]()
    }
    
    // MARK: - Methods
    
    func obtainPosts() {
        for url in urlPool {
            ServiceLayer.shared.rssReader.loadPost(from: url) { [weak self] response in
                guard let strongSelf = self else { return }
                switch response {
                case .fail(_):
                    break
                case .success(let posts):
                    DispatchQueue.main.async {
                        strongSelf.postViewModels.append(posts)
                        strongSelf.delegate?.postDidLoad(forSection: strongSelf.postViewModels.count - 1)
                    }
                }
            }
        }
    }
    
    func isFavorite(post: Post) -> Bool {
        return ServiceLayer.shared.favoritePosts.isFavorite(post: post)
    }
    
}

// MARK: - RSSFeedPresentationModelDelegate

protocol RSSFeedPresentationModelDelegate: AnyObject {
    func postDidLoad(forSection: Int)
}
