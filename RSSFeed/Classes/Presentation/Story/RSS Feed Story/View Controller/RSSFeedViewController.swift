//
//  RSSFeedViewController.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class RSSFeedViewController: UIViewController {

    // MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Property
    
    private let presentationModel: RSSFeedPresentationModel
    private lazy var router = RSSFeedRouter(root: self)
    
    // MARK: - Init
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.presentationModel = RSSFeedPresentationModel(urlPool: rssList)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.presentationModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.presentationModel = RSSFeedPresentationModel(urlPool: rssList)
        super.init(coder: aDecoder)
        self.presentationModel.delegate = self
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
        self.presentationModel.obtainPosts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
        else if let indexPaths = tableView.indexPathsForVisibleRows {
            tableView.beginUpdates()
            tableView.reloadRows(at: indexPaths, with: .automatic)
            tableView.endUpdates()
        }
    }

    // MARK: - Configure
    
    private func configure() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.tableFooterView = UIView()
    }

}

// MARK: - UITableViewDataSource

extension RSSFeedViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.presentationModel.postViewModels.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presentationModel.postViewModels[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: RSSFeedTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RSSFeedTableCell
        let post = self.presentationModel.postViewModels[indexPath.section][indexPath.row]
        cell.configure(fromPost: post)
        cell.backgroundColor = self.presentationModel.isFavorite(post: post) ? .yellow : .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.presentationModel.urlPool[section].absoluteString
    }
    
}

// MARK: - UITableViewDelegate

extension RSSFeedViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.router.showPostDescription(forPost: self.presentationModel.postViewModels[indexPath.section][indexPath.row])
    }
}

// MARK: - RSSFeedPresentationModelDelegate

extension RSSFeedViewController : RSSFeedPresentationModelDelegate {
  
    func postDidLoad(forSection: Int) {
        self.tableView.beginUpdates()
        self.tableView.insertSections([forSection], with: .fade)
        self.tableView.endUpdates()
    }
    
}

