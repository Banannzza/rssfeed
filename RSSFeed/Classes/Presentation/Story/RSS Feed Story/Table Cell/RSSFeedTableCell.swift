//
//  RSSFeedTableCell.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class RSSFeedTableCell: UITableViewCell {

    // MARK: - Outlet
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailImageView.image = nil
        titleLabel.text = nil
    }
    
    // MARK: - Configure
    
    func configure(fromPost post: Post) {
        DispatchQueue.global(qos: .userInteractive).async {
            if let urlString = post.image, let url = URL(string: urlString) {
                if let imageData = try? Data(contentsOf: url) {
                    DispatchQueue.main.async { [weak self] in
                        self?.thumbnailImageView.image = UIImage(data: imageData)
                    }
                }
            }
        }
        titleLabel.text = post.title
    }
    
}
