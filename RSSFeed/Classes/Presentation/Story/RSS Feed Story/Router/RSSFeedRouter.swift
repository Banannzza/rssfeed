//
//  RSSFeedRouter.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 09/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

final class RSSFeedRouter: Router {
    
    func showPostDescription(forPost post: Post) {
        self.show(storyboard: StoryboardIdentifier.topicDescription.rawValue, identifier: "PostDescriptionViewController") { viewController in
            (viewController as! PostDescriptionViewController).post = post
        }
    }
    
    private enum StoryboardIdentifier: String {
        case topicDescription = "PostDescriptionViewController"
    }
    
}
