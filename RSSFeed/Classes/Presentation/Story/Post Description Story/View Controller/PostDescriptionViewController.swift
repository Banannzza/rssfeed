//
//  TopicDescriptionViewController.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 10/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class PostDescriptionViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var contentTitleLabel: UILabel!
    @IBOutlet weak var contentFavoriteButton: UIButton!
    @IBOutlet weak var contentBrowserButton: UIButton!
    
    // MARK: - Property
    
    var post: Post!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateFavoriteButtonTitle()
        self.configure(post: self.post)
    }
    
    func configure(post: Post) {
        self.contentTitleLabel.text = post.title
        DispatchQueue.global(qos: .userInteractive).async {
            if let urlString = post.image, let url = URL(string: urlString) {
                if let imageData = try? Data(contentsOf: url) {
                    DispatchQueue.main.async { [weak self] in
                        self?.contentImageView.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }
    
    // MARK: - Common
    
    private func updateFavoriteButtonTitle() {
        let favoriteButtonTitle = ServiceLayer.shared.favoritePosts.isFavorite(post: self.post) ? "Удалить из закладок" : "Добавить в закладки"
        self.contentFavoriteButton.setTitle(favoriteButtonTitle, for: .normal)
    }
    
    // MARK: - IBAction
    
    @IBAction func browserButtonClicked(_ sender: Any) {
        if let url = URL(string: self.post.link), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        if ServiceLayer.shared.favoritePosts.isFavorite(post: self.post) {
            ServiceLayer.shared.favoritePosts.removeFavorite(post: self.post)
        }
        else {
            ServiceLayer.shared.favoritePosts.setFavorite(post: self.post)
        }
        self.updateFavoriteButtonTitle()
    }
}

