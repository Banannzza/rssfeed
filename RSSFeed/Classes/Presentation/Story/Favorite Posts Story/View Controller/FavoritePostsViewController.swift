//
//  FavoritePostsViewController.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 10/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class FavoritePostsViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Property
    
    private let presentationModel = FavoritePostsPresentationModel()
    private lazy var router = RSSFeedRouter(root: self)
    
    // MARK: - Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        presentationModel.obtainPosts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        presentationModel.obtainPosts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.presentationModel.needReload() {
            tableView.reloadData()
            return
        }
        if let indexPath = tableView.indexPathForSelectedRow {
            if !presentationModel.isFavorite(post: self.presentationModel.postViewModels[indexPath.row]) {
                tableView.beginUpdates()
                presentationModel.removeFavorite(postIndex: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.endUpdates()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
    }
    
    // MARK: - Configure
    
    private func configure() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.tableFooterView = UIView()
    }
    
}

// MARK: - UITableViewDataSource

extension FavoritePostsViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.postViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: RSSFeedTableCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RSSFeedTableCell
        cell.selectionStyle = .none
        cell.configure(fromPost: self.presentationModel.postViewModels[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension FavoritePostsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.router.showPostDescription(forPost: self.presentationModel.postViewModels[indexPath.row])
    }
}
