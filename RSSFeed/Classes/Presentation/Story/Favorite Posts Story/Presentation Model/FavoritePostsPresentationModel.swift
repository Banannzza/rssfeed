//
//  FavoritePostsPresentationModel.swift
//  RSSFeed
//
//  Created by Алексей Остапенко on 10/06/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class FavoritePostsPresentationModel {
    
    // MARK: - Property
    
    var postViewModels = [Post]()
    
    // MARK: - Methods
    
    func obtainPosts() {
        postViewModels = ServiceLayer.shared.favoritePosts.obtainFavoritePosts()
    }
    
    func needReload() -> Bool {
        let lastPosts = ServiceLayer.shared.favoritePosts.obtainFavoritePosts()
        if lastPosts.count != postViewModels.count {
            postViewModels = lastPosts
            return true
        }
        for post in postViewModels {
            if !lastPosts.contains(where: { $0.link == post.link }) {
                postViewModels = lastPosts
                return true
            }
        }
        return false
    }
    
    func isFavorite(post: Post) -> Bool {
        return ServiceLayer.shared.favoritePosts.isFavorite(post: post)
    }
    
    func removeFavorite(postIndex index: Int) {
        guard index < postViewModels.count else { return }
        postViewModels.remove(at: index)
    }
}
